package com.viseo.speedtalks.beers.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.viseo.speedtalks.beers.model.Beer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface BeersRepository extends ReactiveMongoRepository<Beer, String> {

	Flux<Beer> findAllByAbv(Double abv);

	Mono<Beer> findByKey(String key);
}