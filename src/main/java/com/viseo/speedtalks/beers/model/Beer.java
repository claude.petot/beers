package com.viseo.speedtalks.beers.model;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Document(collection = "beers")
public class Beer {

	@Id
	private String key;
	private String title;
	private String synonyms;
	private List<String> tags;
	private Double abv;

	private Brewery brewery;
	private Country country;

	public boolean abvGreaterThan(Double minAbv) {
		return abv != null && abv > minAbv;
	}

	public String getKey() {
		return key;
	}

	public String getTitle() {
		return title;
	}

	public String getSynonyms() {
		return synonyms;
	}

	public List<String> getTags() {
		return tags;
	}

	public Double getAbv() {
		return abv;
	}

	public Brewery getBrewery() {
		return brewery;
	}

	public Country getCountry() {
		return country;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
				.append("title", title)
				.append("abv", abv)
				.append("brewery", brewery)
				.append("country", country)
				.toString();
	}
}
