package com.viseo.speedtalks.beers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viseo.speedtalks.beers.model.Beer;
import com.viseo.speedtalks.beers.repository.BeersRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/api/beers")
public class BeersController {

	@Autowired
	private BeersRepository beersRepository;

	@GetMapping(params = "abv")
	public Flux<Beer> findByAbv(@RequestParam("abv") Double abv) {
		return beersRepository.findAllByAbv(abv);
	}

	@GetMapping("/{key}")
	public Mono<Beer> findByKey(@PathVariable String key) {
		return beersRepository.findByKey(key);
	}

}
