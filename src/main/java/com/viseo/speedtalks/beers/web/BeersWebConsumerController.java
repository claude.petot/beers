package com.viseo.speedtalks.beers.web;

import java.time.Duration;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import com.viseo.speedtalks.beers.model.Beer;
import com.viseo.speedtalks.beers.repository.BeersRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin
@RestController
@RequestMapping("/external/beers")
public class BeersWebConsumerController {

	@Autowired
	private WebClient client;

	@Autowired
	private BeersRepository beersRepository;

	@GetMapping("random")
	public Mono<Beer> getRandomBeer() {
		return client.get()
				.accept(MediaType.APPLICATION_JSON)
				.retrieve()
				.bodyToMono(Beer.class);
	}

	@GetMapping("randoms")
	public Flux<Beer> getRandomBeers() {
		return getRandomBeer()
				.delayElement(Duration.ofMillis(200))
				.repeat();
	}

	@GetMapping
	public Flux<Beer> getAndSaveRandomBeers(@RequestParam Integer nbBeers) {
		return getRandomBeers()
				.take(nbBeers)
				.flatMap(beersRepository::save)
				.log();
	}

	// Autres méthodes intéressantes mais non utilisées côté front

	@GetMapping(params = "abvMin")
	public Flux<Beer> getBeersAbvMin(@RequestParam("abvMin") Double abvMin) {
		return getRandomBeers()
				.filter(b -> b.abvGreaterThan(abvMin));
	}

	@GetMapping(value = "/until", params = "abvMax")
	public Flux<Beer> getBeersUntilAbvMax(@RequestParam("abvMax") Double abvMax) {
		return getRandomBeers()
				.takeUntil(b -> b.getAbv() != null && b.getAbv() >= abvMax);
	}

	@GetMapping("/abv")
	public Flux<Double> getBeersAbv() {
		return getRandomBeers()
				.map(Beer::getAbv)
				.filter(Objects::nonNull);
	}

}
