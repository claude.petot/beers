# Lancement Mongo
sudo mongod &

# Liste des bières
http://openbeer.github.io/

# API
http://prost.herokuapp.com/api/v1

# Liens intéressants
https://tech.willhaben.at/reactive-java-performance-comparison-c4d248c8d21f
https://fr.slideshare.net/fbeaufume/programmation-ractive-avec-spring-5-et-reactor-81924228
https://blog.zenika.com/2016/07/29/quoi-de-neuf-dans-spring-5/https://fr.slideshare.net/fbeaufume/programmation-ractive-avec-spring-5-et-reactor-81924228

##Schéma à reprendre
https://grokonez.com/reactive-programming/angular-4-spring-webflux-spring-data-reactive-mongodb-example-full-reactive-angular-4-http-client-spring-boot-restapi-server#31_Reactive_Service

## Reactive JDBC
Reactive JDBC Java One 2017 http://www.oracle.com/technetwork/database/application-development/jdbc/con1491-3961036.pdf
Oracle AOJ https://github.com/oracle/oracle-db-examples/tree/master/java/AoJ
